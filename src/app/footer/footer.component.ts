import { Component, OnInit } from '@angular/core';
import { faPhone, faFax, faEnvelope, faCoffee, faEnvelopeOpen } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  faPhone = faPhone;
  faFax = faFax;
  faEnvelope = faEnvelope;
  faCoffee = faCoffee;
  faEnvelopeOpen = faEnvelopeOpen;

  ngOnInit(): void {
  }

}
