import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkillComponent } from './components/skill/skill.component';



@NgModule({
  declarations: [
    SkillComponent
  ],
  imports: [
    CommonModule
  ]
})
export class QuizModule { }
